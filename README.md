# Javascript Backend Developer Test
## TEST2
This test is intended for prospective Javascript Developers focusing on Backend side to join PT Lussa Teknologi Global. You need to do this test first before joining our team. Happy Coding and Good Luck!

### Tools
_Express, Node, and MovieDB API Online_

### Preconfigure (Optional)
- Go to **test2** root directory
- Type **npm install**

### How to Run
1) Type **npm start** in root of directory **test2**
2) Here's the API urls, you can access:
- _BASE URL_: http://localhost:3000/api/
- **GET** LIST GENRES: _BASE URL_ + _genre/all_
- **GET** LIST TV GENRES: _BASE URL_ + _genre/all/tv_
- **GET** LIST MOVIE: _BASE URL_ + _movie/all_ **(the query params refers to: <a href='https://developers.themoviedb.org/3/discover/movie-discover'>this</a>)**
- **GET** DETAIL MOVIE: _BASE URL_ + _movie/all/:movieId_ **(example movie_id: 44683)**
- **POST** RATING: _BASE URL_ + _movie/rating/:movieId_ 
**BODY EXAMPLE**: 
`{
`    "value": "1"`
}`
**(example movie_id: 44683, value: 1-5(float) )**
- **DELETE** RATING: _BASE URL_ + _movie/rating/:movieId_ **(example movie_id: 44683)**
3) CHECK benchmark: Just Type **npm test** and see in the console/terminal :)

#### About
- Writter: Abdul Salam
- Email: abdulsalam121196@gmail.com
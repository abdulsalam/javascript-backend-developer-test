var express = require('express');
const { route } = require('.');
var router = express.Router();
const movieController = require('../controllers/MovieController');

router.get('/all', movieController.getMovies);
router.get('/all/:movie_id', movieController.getMovieById);
//
router.post('/rating/:movie_id', movieController.addRating);
//
router.delete('/rating/:movie_id', movieController.deleteRating);

module.exports = router;
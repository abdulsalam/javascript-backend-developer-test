var express = require('express');
var router = express.Router();
const genreController = require('../controllers/GenreController');

// GENRE
router.get('/all', genreController.getGenres);
router.get('/all/tv', genreController.getListTv);

module.exports = router;
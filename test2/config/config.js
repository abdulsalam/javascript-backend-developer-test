const axios = require('axios').default;

module.exports = {
    api3Key: "337a8130d9c5af222948b42f4bb3c85d",
    api4Key: "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIzMzdhODEzMGQ5YzVhZjIyMjk0OGI0MmY0YmIzYzg1ZCIsInN1YiI6IjYwNmZlOWZkM2Y4ZWRlMDAyOTc0OWQyOCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.4uwYcEr1YF3Z23j9L8sLz_Iu7giecxvoNEbwxrcxdXI",
    baseUrl: "https://api.themoviedb.org/3",
    axios,
    session: "session"
}
const apiBenchmark = require('api-benchmark');
const apiKey = "337a8130d9c5af222948b42f4bb3c85d";

// compare with the real api
const services = {
    server1: "http://127.0.0.1:3000/api/",
    server2: "https://api.themoviedb.org/3/"
}

const routes = {
    route1: `movie/all`,
    route2: `discover/movie?api_key=${apiKey}`
}

// log all
apiBenchmark.measure(services, routes, (err, result) => {
    console.log(result);
});
'use strict';

const http = require('../config/config');
const localStorage = require('local-storage');

const _getAuth = async() => {
    const url = `${http.baseUrl}/authentication/token/new?api_key=${http.api3Key}`;
    const authKey = await http.axios.get(url)
        .then((data) => {
            return data.data?.request_token;
        })
        .catch((err) => {
            throw err;
        });

    return authKey;
}

const _getSessionId = async() => {
    const url = `${http.baseUrl}/authentication/session/new?api_key=${http.api3Key}`;
    const authKey = await _getAuth();

    const res = await http.axios.post(url, {request_token: authKey})
        .then((data) => {
            return data.data
        })
        .catch((err) => {
            throw err;
        });

    return res;
}

const _guestMode = async() => {
    if (_getSession()) return _getSession();

    const url = `${http.baseUrl}/authentication/guest_session/new?api_key=${http.api3Key}`;
    const authKey = await http.axios.get(url)
        .then(async(data) => {
            if (_getSession() === null) {
                await _setSession(data.data?.guest_session_id);
            }

            return data.data?.guest_session_id;
        })
        .catch((err) => {
            throw err;
        });

    return authKey;
}

const _setSession = (value) => {
    localStorage.set(http.session, value);
}

const _getSession = () => {
    localStorage.get(http.session);
}

module.exports = {
    getAuth: _getAuth,
    setsession: _setSession,
    getSession: _getSession,
    getSessionId: _getSessionId,
    guestMode: _guestMode
};
'use strict';

const http = require('../config/config');
const auth = require('../controllers/AuthController');

const _getMovies = (req, res, next) => {
    // query by -- 
    /** popularity.asc/desc, original_title.asc/desc, release_date.asc/desc, can refer to:
    // https://developers.themoviedb.org/3/discover/movie-discover
    **/
    const sortBy = req.query.sort_by;
    const page = req.query.page ?? 1;   
    const year = req.query.year;
    const withGenres = req.query.genres;
    const includeAdult = req.query.include_adult;

    const params = {
        page,
        sort_by: sortBy,
        year,
        with_genres: withGenres,
        include_adult: includeAdult
    };

    const url = `${http.baseUrl}/discover/movie?api_key=${http.api3Key}`;
    //
    http.axios.get(url, {params: params})
        .then((data) => {
            res.status(200).json(data.data);
        })
        .catch((err) => {
            res.status(500).json(err);
        }).finally(() => {
            res.end();
        })
}

const _getMovieById = (req, res, next) => {
    const movieId = req.params.movie_id;
    const url = `${http.baseUrl}/movie/${movieId}?api_key=${http.api3Key}`;
    //
    http.axios.get(url)
        .then((data) => {
            res.status(200).json(data.data);
        })
        .catch((err) => {
            res.status(500).json(err);
        }).finally(() => {
            res.end();
        })
}

const _giveRating = async(req, res, next) => {
    const movieId = req.params.movie_id;
    const rating = req.body.value;
    const url = `${http.baseUrl}/movie/${movieId}/rating`;
    const authKey = await auth.guestMode();
    const params = {
        api_key: http.api3Key,
        guest_session_id: authKey
    };

    http.axios.post(url, { value: rating }, { params: params })
        .then((data) => {
            res.status(200).json(data.data);
        })
        .catch((err) => {
            res.status(500).json(err);
        })
        .finally(() => {
            res.end();
        })
}

const _removeRating = async(req, res, next) => {
    const movieId = req.params.movie_id;
    const rating = req.body.value;
    const url = `${http.baseUrl}/movie/${movieId}/rating`;
    const authKey = await auth.guestMode();
    const params = {
        api_key: http.api3Key,
        guest_session_id: authKey
    };

    http.axios.delete(url, { params: params })
        .then((data) => {
            res.status(200).json(data.data);
        })
        .catch((err) => {
            res.status(500).json(err);
        })
        .finally(() => {
            res.end();
        })
}

module.exports = {
    getMovies: _getMovies,
    getMovieById: _getMovieById,
    addRating: _giveRating,
    deleteRating: _removeRating
}
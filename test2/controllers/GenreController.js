'use strict';

const http = require('../config/config');

const _getAll = (req, res, next) => {
    const url = `${http.baseUrl}/genre/movie/list?api_key=${http.api3Key}`;
    //
    http.axios.get(url)
        .then((data) => {
            res.status(200).json(data.data);
        })
        .catch((err) => {
            res.status(500).json(err);
        }).finally(() => {
            res.end();
        })
}

const _getListTv = (req, res, next) => {
    const url = `${http.baseUrl}/genre/tv/list?api_key=${http.api3Key}`;
    //
    http.axios.get(url)
        .then((data) => {
            res.status(200).json(data.data);
        })
        .catch((err) => {
            res.status(500).json(err);
        }).finally(() => {
            res.end();
        })
}

module.exports = {
    getGenres: _getAll,
    getListTv: _getListTv
}
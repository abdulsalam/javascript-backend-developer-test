// scope var
let combArr = [];
let usedNums = [];

const combinationDistinct = (arr = []) => {
  let i, num;
  for (i = 0; i < arr.length; i++) {
    num = arr.splice(i, 1)[0]; // split by char, get 1 char of num
    usedNums.push(num); // push to array of temp used number

    if (arr.length === 0) combArr.push(usedNums.slice());

    // do recursion
    combinationDistinct(arr);

    arr.splice(i, 0, num); // get i char, add num in i position
    usedNums.pop(); // remove from usednumber
  }

  return combArr;
}

// to json string
console.log(JSON.stringify(combinationDistinct([1,2,3]))); //expected: distict numbers possibility
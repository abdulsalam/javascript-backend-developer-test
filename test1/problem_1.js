'use strict';

const findDuplicateElement = (arr = []) => {
  let number = -1;

  const res = arr.some((elem, idx) => {
    const duplicate = arr.indexOf(elem) !== idx;
    if (duplicate) number = elem;

    return duplicate;
  });

  return res ? `duplicate element is: ${number}` : "no duplicate element";
}

console.log(findDuplicateElement([1,1,3,4,5])); // expected: 1
console.log(findDuplicateElement([1,2,3,4,4])); // expected: 4
console.log(findDuplicateElement([1,2,3,3,4])); // expected: 3


const maxProductArray = (arr = []) => {
    if (arr.length === 0) return 0;
  
    let localMax = 0;
    let localMin = 0;
    let lastMax = arr[0]; // assumption - first element is largest
    let lastMin = arr[0]; // assumption - first element is smalest
    let max = arr[0];
    
    for (let i = 1; i < arr.length; i++) {
      // get max value, between last max * arr[i], last min * arr[i], or arr index i
      localMax = Math.max(lastMax * arr[i], lastMin * arr[i], arr[i]);
      // get min value, between last max * arr[i], last min * arr[i], or arr index i
      localMin = Math.min(lastMax * arr[i], lastMin * arr[i], arr[i]);
      // compare with max var
      max = Math.max(max, localMax);
  
      // change the value of last var
      lastMax = localMax;
      lastMin = localMin;
    }
  
    return max;
  };
  
  console.log(maxProductArray([-6,4,-5,8,-10,0,8])); // expected: 1600
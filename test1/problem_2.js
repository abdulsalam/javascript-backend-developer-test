'use strict';

const sortLinear = (arr = []) => {
  return arr.sort((a, b) => a-b);
}

console.log(sortLinear([0,1,2,2,1,0,0,2,0,1,1,0])); // expected: [0,0,0,0,0,1,1,1,1,2,2,2]